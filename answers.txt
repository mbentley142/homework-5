# Lehigh University CSE262 - Programming Languages - Homework 5

Solve the following ten questions regarding the Lambda Calculus.

## Question 1
1. (((λp.pz) (λq.w)) (λw.wqzp))

2. ((λp.pq) (λp.qp))

## Question 2

In the following expressions say which, if any, variables are bound (and to which λ), and which are free.

1. λs'.s' z λq'.s q'
Variables with ' are bound to the corresponding variable with an '. Variables without ' are unbound. 

2. (λs'. s' z) λq'. w λw'. w' q z s
Variables with ' are bound to the corresponding variable with an '. Variables without ' are unbound. 

3. (λs'.s') (λq'.q's)
Variables with ' are bound to the corresponding variable with an '. Variables without ' are unbound. 

4. λz'. (((λs'.s'q) (λq'.q'z')) λz". (z" z"))
Variables with '/" are bound to the corresponding variable with an '/". Variables without ' or " are unbound. 

## Question 3

Put the following expressions into beta normal form (use β-reduction as far as possible, α-conversion as needed) assuming left-association.

1. ((λz.z) (λq.qq)) (λs.sa)
((λz.z)[z -> (λq.qq)]) (λs.sa)
(λq.qq) (λs.sa)
(λq.qq)[q -> (λs.sa)]
(λs.sa) (λs.sa)
(λs.sa)[s -> (λs.sa)]
((λs.sa) a)
((λs.sa) a)[a -> a']
((λs.sa) a')
((λs.sa)[s -> a'])
(a'a)

2. (((λz.z) (λz.zz)) (λz.zq))
(((λz.z) (λz.zz)[z -> z']) (λz.zq)[z -> z"])
(((λz.z) (λz'.z'z')) (λz".z"q))
(((λz.z)[z -> (λz'.z'z')]) (λz".z"q))
((λz'.z'z') (λz".z"q))
((λz'.z'z')[z' -> (λz".z"q)])
(λz".z"q) (λz".z"q)[z" -> z'"]
(λz".z"q) (λz'".z'"q)
(λz".z"q)[z" -> (λz'".z'"q)]
(λz'".z'"q) q
(λz'".z'"q)[q -> q'] q
(λz'".z'"q') q
(qq')

3. ((λsq.sqq) (λa.a)) b
((λsq.sqq)[s -> (λa.a)]) b
(λq.(λa.a)qq) b
(λq.(λa.a)qq)[q -> b]
((λa.a) b b)
((λa.a)[a -> b] b)
(bb)

4. ((λsq.sqq) (λq.q)) q
((λsq.sqq) (λq.q)[q -> q']) q[q -> q"]
((λsq.sqq) (λq'.q')) q"
((λsq.sqq)[s -> (λq'.q')]) q"
(λq.(λq'.q')qq) q"
(λq.(λq'.q')qq)[q -> q"]
((λq'.q') q" q")
((λq'.q')[q' -> q"] q")
(q"q")

5. ((λs.ss) (λq.q)) (λq.q)
((λs.ss)[s -> (λq.q)]) (λq.q)[q -> q']
((λq.q) (λq.q)) (λq'.q')
((λq.q)[q -> (λq.q)]) (λq'.q')
(λq.q) (λq'.q')
(λq.q)[q -> (λq'.q')]
(λq'.q')

## Question 4

1. Write the truth table for the or operator below.

T, T -> T
T, F -> T
F, T -> T
F, F -> F

2. The Church encoding for OR = (λp.λq.p p q)

Prove that this is a valid "or" function by showing that its output matches the truth table above. You will have 4 derivations. For the first derivation, show the long-hand solution (don't use T and F, use their definitions). For the other 3 you may use the symbols in place of the definitions. 

T, T -> T
(λpq.ppq) λxy.x λxy.x
(λpq.ppq)[p -> λxy.x] λxy.x
(λq.(λxy.x)(λxy.x)q) λxy.x
(λq.(λxy.x)(λxy.x)q)[q -> λxy.x]
(λxy.x) (λxy.x) (λxy.x)
(λxy.x)[x -> (λxy.x)] (λxy.x)
(λy.(λxy.x)) (λxy.x)
(λy.(λxy.x))[y -> (λxy.x)]
(λxy.x)
T

T, F -> T
(λpq.ppq) T F
(λpq.ppq)[p -> T] F
(λq.TTq) F
(λq.TTq)[q -> F]
(T T F)
T

F, T -> T
(λpq.ppq) F T
(λpq.ppq)[p -> F] T
(λq.FFq) T
(λq.FFq)[q -> T]
(F F T)
T

F, F -> F
(λpq.ppq) F F
(λpq.ppq)[p -> F] F
(λq.FFq) F
(λq.FFq)[q -> F]
(F F F)
F

## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement.

T -> F
F -> T

T -> F
(λp.p(λxy.y)(λxy.x)) λxy.x
(λp.p(λxy.y)(λxy.x))[p -> λxy.x]
(λxy.x) (λxy.y) (λxy.x)
(λxy.x) (λxy.y) (λxy.x) [x -> (λxy.y)]
(λy.(λxy.y)) (λxy.x)
(λy.(λxy.y))[y -> λxy.x]
(λxy.y)
F

F -> T
(λp.p(λxy.y)(λxy.x)) λxy.y
(λp.p(λxy.y)(λxy.x))[p -> λxy.y]
(λxy.y) (λxy.y) (λxy.x)
(λxy.y) (λxy.y) (λxy.x) [x -> (λxy.y)]
(λy.y) (λxy.x)
(λy.y)[y -> λxy.x]
(λxy.x)
T

It works similar to an if statement since it will return the first branch (false) if its true, and otherwise it will return the second branch (true). 